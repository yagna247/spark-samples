package com.zeyobron

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

object SparkWriteStreaming {
  
  def main(args: Array[String]): Unit = {
    
    
    val sparkConf = new SparkConf()
                  .setAppName("streamingWrite")
                  .setMaster("loca[*]")
                  
      val sparkContext =  new SparkContext(sparkConf)
    
    sparkContext.setLogLevel("ERROR")
    
    val spark = SparkSession.builder().getOrCreate()
    import spark.implicits._
    
    //	Task 1 ---- Kafka Write Stream
    
    val df = spark
              .readStream
              .format("kafka")
					.option("kafka.bootstrap.servers","localhost:9092")
					.option("subscribe","yagna-0")
					.load()
					.withColumn("value", expr("cast(value as string)"))
					
					
					
					
					df.writeStream
					   
    
    
    
  }
}