package com.zeyobron


import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._

object TestStreaming {
  
  
  def main(args:Array[String]):Unit={

			val conf = new SparkConf().setAppName("First").setMaster("local[*]")
					val sc = new SparkContext(conf)
					sc.setLogLevel("ERROR")

					val spark = SparkSession.builder().getOrCreate()
					import spark.implicits._

					val weblogschema = StructType(Array(
						StructField("name", StringType, true),
							StructField("amount", StringType, true)));  

			
			val df = spark.readStream.format("csv")
			         .schema(weblogschema)
			         .load("file:///D://BigData//Data//Test")
			         
			         
			    df.writeStream.format("console")
			    .option("checkpointLocation","file:///D://BigData//Data//Test1" )
			    .start()
			    .awaitTermination()
			 

	}
}