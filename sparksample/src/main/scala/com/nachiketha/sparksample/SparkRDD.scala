package com.nachiketha.sparksample

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import scala.tools.scalap.Main

object SparkRDD {
  
 def main(args: Array[String]): Unit = {
   
 
  //Task 1 --


//Read txns file 
  
  val conf = new SparkConf().setAppName("First").setMaster("local[*]")
    val sc =  new SparkContext(conf)
    sc.setLogLevel("ERROR")
    
   val txns = sc.textFile("file:///D://BigData//txns", 1);
  
  
 // println("txns data"+ txns.max());
//Filter cash rows consist of Cash
  
val data = sc.textFile("file:///D://Bigdata//txns")			
val cashdata = data.filter(x=>x.contains("cash"))
val flatdata = cashdata.flatMap(x=>x.split(","))
flatdata.take(10).foreach(println)

 
  

//Task 2 -- 

//From output from Task 1 ---- Append word ",zeyo"
//From the output of before line ---- Iterate each row--- Replace zeyo with zeyobron

val appendata = flatdata.map(x=>x+",zeyo")
println("=======append data====")
appendata.take(10).foreach(println)
println("=======replace data====")
val replacedata = appendata.map(x=>x.replace("zeyo","zeyobron"))
replacedata.take(10).foreach(println)
 
     

//Task 2 -- 

//From output from Task 1 ---- Append word ",zeyo"
//From the output of before line ---- Iterate each row--- Replace zeyo with zeyobron
  }
  
}