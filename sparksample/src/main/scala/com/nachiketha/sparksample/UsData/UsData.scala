package com.nachiketha.sparksample.UsData

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext


object Usadata {
  
  def main(args: Array[String]): Unit = {
    
    val conf = new SparkConf().setAppName("First").setMaster("local[*]")
    val sc =  new SparkContext(conf)
    sc.setLogLevel("ERROR")
    
    
  //  Read the data  ( you have txt extension )

    println()
    println("---------------load Data- usdata---------------")
    println()
    val txnsGymData =  sc.textFile("file:///D:/BigData/Data/usdata.csv", 1);
    

    //Do map split
       println()
    println("---------------Do MapSplit---------------")
    println()
  val mapsplitData =  txnsGymData.map(st => st.split(","))
    
//Filter 5th split with Gymnastics
   println()
    println("---------------Filter 5th split with Gymnastics---------------")
    println()
  val gymdata =mapsplitData.filter(str => str.length >200)
  
  gymdata.foreach(println)
  
//Do mkstring
  println()
    println("---------------MK String---------------")
    println()
  val mkstrGymData = gymdata.map(str => str.mkString(","))
  
  
  
//Print the result rdd
  mkstrGymData.foreach(println)
  
    
   // val gymData = data.filter(x => x.contains("Gymnastics"))
   
    
  }
}