package com.nachiketha.sparksample

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import sys.process._


object SparkObj {
  
  def main(args: Array[String]): Unit = {
    
       
    val conf = new SparkConf().setAppName("First").setMaster("local[*]")
    val sc =  new SparkContext(conf)
    sc.setLogLevel("ERROR")
    
    var data =  sc.textFile("file:///home/cloudera/data/txns", 1);
    
   // val data = sc.textFile("file:///D://BigData//txns", 1)
    
    val gymData = data.filter(x => x.contains("Gymnastics"))
    
    "hadoop fs -rmr /user/cloudera/gymdata/".!
    
    
    gymData.saveAsTextFile("/user/cloudera/gymdata");
    
    
  }
  
}