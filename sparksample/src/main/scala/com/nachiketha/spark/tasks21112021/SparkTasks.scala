package com.nachiketha.spark.tasks21112021

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types._


object SparkTasks {
  
  
  def main(args: Array[String]): Unit = {
    
    
    
    /*
     * Task 1 ---

Have txns_gym2.txt
Define Struct Type as 

	val structSchema = StructType(Array(
	StructField("txnno",StringType),
	StructField("category",StringType),
	StructField("product",StringType)
	))
Create dataframe using dataframe read
val df = spark.read.format("csv").schema(structSchema).load("path to txns_gym2.txt")
     */
    
    val sparkConf = new SparkConf().setAppName("Spark Task1").setMaster("local[*]")
    
    val sparkContext = new SparkContext(sparkConf)
    
    sparkContext.setLogLevel("ERROR")
    
   val spark = SparkSession.builder().getOrCreate()
   import spark.implicits._
  
  val structSchema = StructType(Array(
      StructField("txnno", StringType),
      StructField("category",StringType),
	    StructField("product",StringType)
	))
 val csvdf = spark.read.format("csv").schema(structSchema).load("file:///D:/BigData/Data/txns_gym2.txt")
	
    csvdf.show();
    
    
     /*
   * Task 2 ----

Have devices.json file
Read this file using dataframe reads.
spark.read.format("json").load("path to devices.json")
Register it as tempview
Shoot a query for lat>40
   */
    
    
     val jsondf = spark.read.format("json").load("file:///D:/BigData/Data/devices.json")
	
    jsondf.show();
     
     val devices =jsondf.createOrReplaceTempView("devices")
     
    val lat40 = spark.sql("select * from devices where lat >40");
    
     lat40.show()
    
  }
  
  
 
  
  
}