package com.nachiketha.spark.tasks28112021

import org.apache.spark.SparkConf
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession

object SparkXmlAndJDBC {
  
  def main(args: Array[String]): Unit = {
    
    val conf = new SparkConf().setAppName("Tasks 28-11-2021").setMaster("local[*]")
    val context = new SparkContext(conf).setLogLevel("ERROR")
    val spark = SparkSession.builder().getOrCreate()
    
   import spark.implicits._
   
   /*
    * Task 1 ----

add spark xml jar
read book.xml with rowTag as book
show it
    */
    println()
    println("---------TASK1 ---read book.xml with rowTag as book--------")
    println()
   
   val booksData = spark.read
                         .format("com.databricks.spark.xml")
                         .option("rowTag", "book")
                         .load("file:///D:/BigData/Data/book.xml")
                         
      booksData.show()
      booksData.printSchema()
      
     /* Task 2 ---

Read usdata.csv with header true

Write the same usdata.csv with Partitions for county,state*/
      
        println()
    println("---------TASK2 ---Read usdata.csv with header true"
          +"---Write the same usdata.csv with Partitions for county,state--------")
    println()
   
      
      val usData = spark.read
                        .format("csv")
                        .option("header", "true")
                        .load("file:///D:/BigData/Data/usdata.csv")
              
          val partitionnData = usData.write
                                     .format("csv")
                                     .partitionBy("county","state")
                                     .save("file:///D:/BigData/Data/usdatapartition")
                        
                        


/*Task 3 ----

Read usdata.csv with header true
Select only first_name and last_name*/

    println()
    println("---------TASK3 ---Read usdata.csv with header true"
          +"---Select only first_name and last_name--------")
    println()
                                     
     val usDataWithfname = spark.read
                        .format("csv")
                        .option("header", "true")
                        .load("file:///D:/BigData/Data/usdata.csv")
                        .select("first_name", "last_name")
    
    
    usDataWithfname.show()
    
    
    
    
    /*
     * Optional Task 4 -----


Read transactions.xml with rowTag as POSLog
df.printSchema()  --- Dont get panic -- Cult of Complex data
     */
    
    println()
    println("---------TASK4 ---Read transactions.xml with rowTag as POSLog"
          +"---df.printSchema()  --- Dont get panic -- Cult of Complex data--------")
    println()
    
    val transactionData = spark.read
                               .format("com.databricks.spark.xml")
                               .option("rowTag", "POSLog")
                               .load("file:///D:/BigData/Data/transactions.xml")
                               
                               transactionData.printSchema()
                               
  }
  
}