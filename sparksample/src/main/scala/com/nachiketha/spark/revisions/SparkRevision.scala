package com.nachiketha.spark.revisions

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession

object SparkRevision {
  
  case class Schema(
        txnNo:Double, //index(0)
      txndate: String, //index(1)
      custNo: String, //index(2)
      amount : Double, //index(3)
      category: String,//index(4)
      product: String, //index(5)
      city : String, //index(6)
      state: String, // index(7)
      spendBy: String //index(8)
      )
  
  def main(args: Array[String]): Unit = {
    
    
    println(" Rev-task-1#### create List with 1,4,67, and do an itration and add2 to it -start")
    
    
    val numlist = List(1,4,6,7)
    
    numlist.map(i => i+2)
    
    numlist.foreach(println)
    
     var list1 = List("A","B")
    //list1(1)="AA" //Errro
    val  list2 = list1.map(_.toLowerCase())

    println(list1.mkString(","))
    list1.foreach(f=>println(f))
    list1.foreach(println(_))

    println("Reading a value form Index :"+list1(1))
    println("Adding element 'C' to List")
    //list1 += "D"
    list1 = "C" :: list1    // re-assigning
    println(list1.mkString(","))

    println("Adding two Lists")
    var list3 = list1 ::: list2

    list3 :::= list2
    println(list3.mkString(","))

    println("Adding literal to each element in Lists")
    val list4 = list1.map(f=>f+"->")
    println(list4.mkString(","))

    println("Convert all list elements to Int")
    val list5 = List("1","2","3","4","5")
    println(list5.map(f=>f.toInt).mkString(","))

    
      println(" Rev-task-1#### create List with 1,4,67, and do an itration and add2 to it -end")
    
    println(" Rev-task-2#### create List with Zeyo, Zeyobron, Analytics and filter elements contains zeyo--Start")
    val strList = List("zeyo","zeyobron","analytics")
    
    
    strList.filter(str => str.contains("zeyo"))
    strList.foreach(println)
    
     println(" Rev-task-2#### create List with Zeyo, Zeyobron, Analytics and filter elements contains zeyo--End")
    
    
     println(" Rev-task-3#### Read file1 as an rdd and filter gymnastics rows --Start")
   val sparkConf = new SparkConf().setAppName("Spark Revision").setMaster("local[*]")
    
    val sparkContext = new SparkContext(sparkConf)
   sparkContext .setLogLevel("ERROR")
   
    
    print()
   println("-----------------Generated  RDD------" )
   print()
    val dataRDD = sparkContext.textFile("file:///D:/BigData/revisions/file1.txt", 1);
    
  val splitMap =  dataRDD.map(st => st.split(",")) 
 //ejbgccvdvcettkuctfglkdbtjedtnkkvnukbjlndcjtd
  splitMap.foreach(print)
    
     val flatmapfil   =splitMap.filter(str =>str.equals("gymnastics") )
     flatmapfil.foreach(println);
  
   println(" Rev-task-3#### Read file1 as an rdd and filter gymnastics rows --End")
   
   //Create a case class and Impose case class to it And filter product contains Gymnastics? 

   print("Rev-task-4$ ##Create a case class and Impose case class to it And filter product contains Gymnastics?  -Start")
   
   
   
 val mapRDD =splitMap.map(index => Schema(index(0).toDouble,index(1),index(2),index(3).toDouble,index(4),index(5),index(6),index(6),index(7)))
 
 mapRDD.foreach(println)
 
  val spark =   SparkSession.builder().getOrCreate()
    import spark.implicits._
    
    
 val gymDF = mapRDD.toDF()
 
 gymDF.createOrReplaceTempView("gymView")
 
 val  filterGm =spark.sql("select * from gymView where product like '%Gymnastics%'")
 
 filterGm.show()
 
   
   
    
  
    
   
    
  }
  
}