package com.nachiketha.spark.safemodes

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession

object AvroTask {
  
  
  
  
  /*
   * Task  ---
Read us_short.csv
Write to the directory (any drive)  to directory data_write (eg- windows -- file:///C:/data/data_write)
Check with Error --- df.write.format("json").mode("error").save("file:///C:/data/data_write")
Check with append --- df.write.format("json").mode("append").save("file:///C:/data/data_write")
Check with overwrite --- df.write.format("json").mode("overwrite").save("file:///C:/data/data_write")
Check with ignore --- df.write.format("json").mode("ignore").save("file:///C:/data/data_write")
   * /
   */
  
  
 def main(args: Array[String]): Unit = {
   
   
   
   
  val sparkconf  = new SparkConf().setAppName("Safemodes").setMaster("local[*]")
  
    val context =new  SparkContext(sparkconf)
    context.setLogLevel("ERROR")   
    
   val spark = SparkSession.builder().getOrCreate()
   import spark.implicits._
   
   val usshortData = spark.read
                           .format("csv")
                           .option("header", "true")
                           .load("file:///D:/BigData/Data/us_short.csv")
         usshortData.show()
                           
             //Check with Error --- df.write.format("json").mode("error").save("file:///C:/data/data_write")              
        /* val write = usshortData.write 
                                .format("json")
                                .mode("error")
                                .save("file:///D:/BigData/Data/data_write")*/
         //Check with append --- df.write.format("json").mode("append").save("file:///C:/data/data_write")           
         val writeappend = usshortData.write 
                                .format("json")
                                .mode("append")
                                .save("file:///D:/BigData/Data/data_write")
                                
                                
           //Check with overwrite --- df.write.format("json").mode("overwrite").save("file:///C:/data/data_write")           
         val writeoverwrite = usshortData.write 
                                .format("json")
                                .mode("overwrite")
                                .save("file:///D:/BigData/Data/data_write")     
                                
                                
         //  Check with ignore --- df.write.format("json").mode("ignore").save("file:///C:/data/data_write")                     
         val writeignore = usshortData.write 
                                .format("json")
                                .mode("ignore")
                                .save("file:///D:/BigData/Data/data_write")                             
   
 }
  
  
}