package com.nachiketha.spark.xml

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession

object XmlRead {
  
def main(args: Array[String]): Unit = {
  
  val conf = new SparkConf().setAppName("Xml Reading").setMaster("local[*]")
  
  val context = new SparkContext(conf).setLogLevel("ERROR")
  
  val spark = SparkSession.builder().getOrCreate();
      import  spark.implicits._

  val xmlData = spark.read
                    .format("com.databricks.spark.xml")
                    .option("rowTag", "book")
                    .load("file:///D:/BigData/Data/book.xml")
  xmlData.show();
      
  
  
}
  
}