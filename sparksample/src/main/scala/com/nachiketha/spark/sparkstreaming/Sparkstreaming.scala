package com.nachiketha.spark.sparkstreaming

import org.apache.spark._
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.streaming._
import org.apache.spark.streaming._

object Sparkstreaming {
  
  def main(args:Array[String]):Unit={
    
    val conf = new SparkConf().setAppName("data").setMaster("local[*]").set("spark.driver.allowMultipleContexts","true")
			val sc = new SparkContext(conf)
			sc.setLogLevel("Error")

			val spark = SparkSession.builder()
			.getOrCreate()

			import spark.implicits._
			

			val ssc = new StreamingContext(conf,Seconds(1))
			
	
      val stream = ssc.textFileStream("file:///C:/datastream")
      
      
      stream.print()
      
      
      ssc.start()
      ssc.awaitTermination()  
      
			
      
  }
  
}