package com.nachiketha.spark.tasks04122021

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._

object Task04122021 {

	def main(args: Array[String]): Unit = {



			println()
			println("---------------Intiate the SPARK Conf---------------")
			println()

			val conf = new SparkConf().setAppName("Row RDD Mapping").setMaster("local[*]")
			val sc =  new SparkContext(conf)
			sc.setLogLevel("ERROR")

			val spark  = SparkSession.builder().getOrCreate()
			import spark.implicits._


			val df = spark.read
			              .format("csv")
			              .option("header", "true")
			              .load("file:///D:/BigData/Data/txns_head")
			//  Task 1 -------

			//Read txns_head
			//Using selectExpr -- split and get the year
			val condfSplit =df.selectExpr(
					"txnno",
					"split(txndate,'-')[2] as year",
					"custno",
					"amount",
					"category",
					"product",
					"city",
					"state",
					"spendby"
					)
			condfSplit.show(50)




			//Task 2  ------ Perform the same split operation using withColumn --

			val withcloum =	df.withColumn("year",expr("split(txndate,'-')[2]"))

			withcloum.show(50)

			df.unpersist()

	}


}