package com.nachiketha.spark.readandwrite

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession

object ReadwriteObject {
  
  
def main(args: Array[String]): Unit = {
  
  
   println()
    println("---------------Intiate the SPARK Conf---------------")
    println()
     
      val conf = new SparkConf().setAppName("Row RDD Mapping").setMaster("local[*]")
    val sc =  new SparkContext(conf)
    sc.setLogLevel("ERROR")
    
    val spark  = SparkSession.builder().getOrCreate()
    import spark.implicits._
    
    
    
    /*
     * Read US_Short.csv using header true
Create Tempview
Process age > 25
procdf Write it as ORC
Run it again (It will fail) -- Then remove ORC write


Once Done

Write procdf as json to different directory AND REMOVE This code

Once done 

write procdf as parquet to different directory
     */
    
   val usshort = spark.read
                      .format("csv")
                      .option("header", "true")
                      .load("file:///D:/Bigdata/Data/US_Short.csv")
                      
                      usshort.show()
   
  val  usdata = usshort.createOrReplaceTempView("usdata")
  
 val Age25 = spark.sql("select * from usdata where age >25")
 
 Age25.show()
 
 // write 
 usshort.write
        .format("orc")
        .save("file:///D:/Bigdata/Data/orcd")
  
        //Once Done

//Write procdf as json to different directory AND REMOVE This code

  
   usshort.write
        .format("json")
        .save("file:///D:/Bigdata/Data/json")
   
 //  Once done 

//write procdf as parquet to different directory
        
        usshort.write
        .format("parquet")
        .save("file:///D:/Bigdata/Data/parquet")
    
  }
}


