package com.nachiketha.spark.readandwrite.avro

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession

object Avroread {
  
  def main(args: Array[String]): Unit = {
    
     println()
    println("---------------Intiate the SPARK Conf---------------")
    println()
     
      val conf = new SparkConf().setAppName("Row RDD Mapping").setMaster("local[*]")
    val sc =  new SparkContext(conf)
    sc.setLogLevel("ERROR")
    
    val spark  = SparkSession.builder().getOrCreate()
    import spark.implicits._
    
    
    /**
     * Avero



1) read US_Short.csv as csv with dataframe -- csvdf
2) Go to the Below Link and download the Jar
	https://mvnrepository.com/artifact/com.databricks/spark-avro_2.11/4.0.0
3) Add that downloaded jar to your build Path

	csvdf.write.format("com.databricks.spark.avro").mode("overwrite").save("file:///C:/data/avrod")
     */
    
   val usData = spark.read
         .format("csv")
         .load("file:///D:/BigData/Data/us_short.csv")
    usData.show()
    
   /* usData.write
          .format("com.databricks.spark.avro")
          .mode("overwrite")
          .save("file:///D:/BigData/Data/avro")*/
          
          
    val avroread = spark.read
                        .format("com.databricks.spark.avro")
                       .load("file:///D:/BigData/Data/avro/part-00000-c5b78a1a-b476-4bdb-abc6-61c1411c7033-c000.avro") 
                       
            avroread.show()           
                        
   }  
  
}