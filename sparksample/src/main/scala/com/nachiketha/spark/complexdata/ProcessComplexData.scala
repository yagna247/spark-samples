package com.nachiketha.spark.complexdata

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

object ProcessComplexData {
  
  
  def main(args: Array[String]): Unit = {
    
    
    val sparkConf = new SparkConf().setAppName("complex data").setMaster("local[*]")
    
      val sparkContext = new SparkContext(sparkConf).setLogLevel("ERROR")

    val spark = SparkSession.builder().getOrCreate()
    import spark.implicits._
    
    val jsonData = spark.read
                      .format("json")
                      .option("multiline", "true")
                      .load("file:///D:/BigData/Data/complex/comjson.json");
    
    print ("-------------Raw Data Frame  ------")
    
    jsonData.show()
    jsonData.printSchema()
    
    print ("-------------explode with complex Data Frame  ------")
    
   val flattenDF = jsonData.withColumn("Students", explode(col("Students")))
                            .select(
                             "Students.user.*",
                             "address.*",
                             "amount",
                             "orgName",
                             "trainer"
                            )
                            
              flattenDF.show()
              flattenDF.printSchema()
                            
          print("-------ComplexDF from FlattenDF----")                   
           val complexDF= flattenDF.select(
               col("location"),
                col("name"),
                struct(
                    col("PermanentAddress"), 
                    col("TemporaryAddress"))
                    .alias("address"),
                    
                 col("amount"), 
                 col("orgName"),
                 col("trainer")
                               
           )  
              
         complexDF.show()
         complexDF.printSchema()
         
   val complexFinalDF = complexDF
             .groupBy("address","name","amount","trainer")      
             .agg(collect_list(
                 struct( 
                     struct(
                         col("location"), 
                        col("name")
                        .alias("user")
                        )
                       )
                      ).alias("Students")
                      )
                      
            complexFinalDF.show()
            complexFinalDF.printSchema()
  }
                       
          
    
  
}