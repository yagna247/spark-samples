package com.nachiketha.spark.schema

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext




object SchemaSpark {
 
  
  case class schema (
      txnNo:Int,
      txndate: String,
      custNo: String,
      amount : Double,
      category: String,
      product: String,
      city : String,
      state: String,
      spendBy: String
      
    
  )
  
def main(args: Array[String]): Unit = {
  
  
    val conf = new SparkConf().setAppName("First").setMaster("local[*]")
    val sc =  new SparkContext(conf)
    sc.setLogLevel("ERROR")
    
    /*
     * Read the data  ( you have txt extension )
Do map split
Filter 5th split with Gymnastics
Do mkstring
Print the result rdd
     */
    
   //  Read the data  ( you have txt extension )

    println()
    println("---------------load Data- txns_gym1---------------")
    println()
    val txnsGymData =  sc.textFile("file:///D:/BigData/Data/txns_gym1.txt", 1);
    
    txnsGymData.foreach(println)
    

    //Do map split
       println()
    println("---------------Do MapSplit---------------")
    println()
  val mapsplitData =  txnsGymData.map(st => st.split(","))
    
//Filter 5th split with Gymnastics
   println()
    println("---------------Filter 5th split with Gymnastics---------------")
    println()
 val gymdata = mapsplitData.filter(index => index(5).contains("Gymnastics"))
 
  
//Do mkstring
   println()
    println("---------------DO mkString---------------")
    println()
 val mkStringData = gymdata.map(mkstr => mkstr.mkString(","))
 

  
//Print the result rdd
 println()
    println("---------------Print the result rdd---------------")
    println()
  mkStringData.foreach(println)
    
  
   
  
    
  
}
  
}

