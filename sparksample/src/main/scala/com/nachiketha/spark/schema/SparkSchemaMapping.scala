package com.nachiketha.spark.schema

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession

object SparkSchemaMapping {
  
   case class schema (
      txnNo:Int, //index(0)
      txndate: String, //index(1)
      custNo: String, //index(2)
      amount : Double, //index(3)
      category: String,//index(4)
      product: String, //index(5)
      city : String, //index(6)
      state: String, // index(7)
      spendBy: String //index(8)
      
    
  )
  
def main(args: Array[String]): Unit = {
  
      println()
    println("---------------Intiate the SPARK Conf---------------")
    println()
     
       val conf = new SparkConf().setAppName("Schema Mapping").setMaster("local[*]")
    val sc =  new SparkContext(conf)
    sc.setLogLevel("ERROR")
    
     println()
    println("---------------Intiate the SPARK Session---------------")
    println()
    
     //Intiate the Spark Sql
    
     val session = SparkSession.builder().getOrCreate();
       import session.implicits._
    
    //  Read the data  ( you have txt extension )

    println()
    println("---------------load Data- txns_gym1---------------")
    println()
    val txnsGymData =  sc.textFile("file:///D:/BigData/Data/txns_gym1.txt", 1);
    
    txnsGymData.foreach(println)
    
    
   val mapsplitData = txnsGymData.map(sr => sr.split(","))
   
    println()
    println("---------------Schema Mappinng---------------")
    println()
   
   
  val schemaRdd = mapsplitData.map(index => schema(index(0).toInt,index(1),index(2),index(3).toDouble,index(4),index(5),index(6),index(7),index(8)))
  
    schemaRdd.foreach(println)
      
        println()
    println("---------------Covert to data frame---------------")
    println()
    val gymdataframe=  schemaRdd.toDF();
    
    gymdataframe.show();
      println()
    println("---------------create Or Replace Temp View---------------")
    println()
    
    gymdataframe.createOrReplaceTempView("gymdataframe")
    
     println()
    println("---------------Query the dataframe---------------")
    println()
    
    val filterData = session.sql("select * from gymdataframe where product like '%Gymnastics%' ")
    
    filterData.show()
    
     
   
   }
}