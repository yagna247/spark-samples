package com.nachiketha.spark.readformula

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession

object SparkReadFormula {
  
   def main(args: Array[String]): Unit = {
  
      println()
    println("---------------Intiate the SPARK Conf---------------")
    println()
     
      val conf = new SparkConf().setAppName("Row RDD Mapping").setMaster("local[*]")
    val sc =  new SparkContext(conf)
    sc.setLogLevel("ERROR")
    
    val spark  = SparkSession.builder().getOrCreate()
    import spark.implicits._
    
      println()
    println("-------------- #### Formula ####-Read  format load options  show---------------")
    println()
    
      println()
    println("---------------Read  ORC---------------")
    println()
    
    val orcdata = spark.read.format("orc").load("file:///D:/BigData/Data/part_orc.orc")
     orcdata.show()
    
       println()
    println("---------------Read  parquet---------------")
    println()
     
    val parqdata = spark.read.format("parquet").load("file:///D:/BigData/Data/part_par.parquet")
     parqdata.show()
     
    
   }
}