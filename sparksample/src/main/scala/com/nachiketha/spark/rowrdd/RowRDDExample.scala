package com.nachiketha.spark.rowrdd

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.Row
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types._



object RowRDDExample {
  
  
  
  def main(args: Array[String]): Unit = {
  
      println()
    println("---------------Intiate the SPARK Conf---------------")
    println()
     
      val conf = new SparkConf().setAppName("Row RDD Mapping").setMaster("local[*]")
    val sc =  new SparkContext(conf)
    sc.setLogLevel("ERROR")
    
    val session  = SparkSession.builder().getOrCreate()
    import session.implicits._
    
      println()
    println("---------------Read File---------------")
    println()
     
    val data = sc.textFile("file:///D:/BigData/Data/txns_gym2.txt", 1);
    
     val mapsplit = data.map(Str => Str.split(","))
     
    val structSchema = StructType(Array(
						StructField("txnno",StringType),
						StructField("category",StringType),
						StructField("product",StringType)
						))
			  val rowrdd= mapsplit.map( x => Row(x(0),x(1),x(2)))
			  rowrdd.foreach(println)
			  val df = session.createDataFrame(rowrdd, structSchema)
			  df.show()
			  df.createOrReplaceTempView("rowdf")
			  val filterdata = session.sql("select * from rowdf where product like '%Gymnastics%'")
			  println("=========filtered dataframe===========")
			  filterdata.show()
    
    
    
  }

}