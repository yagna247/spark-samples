package com.nachiketha.spark.dslwithcolumn

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.Row



object EmployeeSal {
  
  
  def main(args: Array[String]): Unit = {
    
    val sparkConf = new SparkConf()
                      .setMaster("local[*]")
                      .setAppName("emp sal")
                      
            val sparkContext = new SparkContext(sparkConf)
    
    val spark = SparkSession.builder().getOrCreate()
    import spark.implicits._
  val empDF = spark.read
                    .format("csv")
                    .option("header", "true")
                    .load("file:///D:/Bigdata/Data/emp.csv")
   //empDF.persist()
   empDF.show()
 val prcDF =  empDF.filter(col("emp_dept") === "IT")
                 .withColumn("emp_salary", col("emp_salary") *10) 
    prcDF.show()
                          
  
    
 /* val empRDD =  spark.sparkContext.textFile("file:///D:/Bigdata/Data/emp.csv", 1)
  
  
   val mapsplit = empRDD.map(str => str.split(","))
   
   
    val structSchema = StructType(Array(
						StructField("emp_id",IntegerType),
						StructField("emp_name",StringType),
						StructField("emp_salary",IntegerType),
						StructField("emp_dept",StringType)
						))
						
	 val rowrdd= mapsplit.map( x => Row(x(0),x(1),x(2),x(3)))
			  rowrdd.foreach(println)
			  val df = spark.createDataFrame(rowrdd, structSchema)
			  df.show()
			  df.createOrReplaceTempView("rowdf")		
			   val filterdata = spark.sql("select * from rowdf where emp_dept like '%IT%'")
			  println("=========filtered dataframe===========")
			  filterdata.show()
    */
			  
   
   //split.foreach(println)
    
   // df.show()
      
     // filterDF.show()
  
  
  
  }
  
}