package com.nachiketha.spark.dslwithcolumn

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import org.apache.spark.sql.types._

object Withcolumn {


	def main(args: Array[String]): Unit = {

			println()
			println("---------------Intiate the SPARK Conf---------------")
			println()

			val conf = new SparkConf().setAppName("Row RDD Mapping").setMaster("local[*]")
			val sc =  new SparkContext(conf)
			sc.setLogLevel("ERROR")

			val spark  = SparkSession.builder().getOrCreate()
			import spark.implicits._


			val Df=   spark.read
			              .format("csv")
			              .option("header", "true")
			              .load("file:///D:/BigData/Data/txns_head")
			              
			              Df.persist()
     
			        val Df1 =  Df.withColumn("spendby",
			                    expr("case when spendby='credit' then 1 else 0 end"))
	                        	.withColumn("txnDate",expr("split(txndate,'-')[2]"))
	                         .withColumnRenamed("txnDate", "year")
	                         
	                        	Df1.show(50)
	                        	
	                        	
	                        	
	              val aggDF = Df.groupBy("category")
	                            .agg(
	                                sum("amount")
	                                .cast(DecimalType(18,2))
	                                .alias("category_amount")
	                            )
	                            
	                            aggDF.show(50)
	                        	

	}
}