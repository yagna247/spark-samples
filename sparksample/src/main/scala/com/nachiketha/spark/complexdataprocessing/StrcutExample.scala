package com.nachiketha.spark.complexdataprocessing

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

object StrcutExample {
 
  def main(args: Array[String]): Unit = {
    
    	println()
			println("---------------Intiate the SPARK Conf---------------")
			println()

			val conf = new SparkConf().setAppName("Row RDD Mapping").setMaster("local[*]")
			val sc =  new SparkContext(conf)
			sc.setLogLevel("ERROR")

			val spark  = SparkSession.builder().getOrCreate()
			import spark.implicits._
    
    /*
     * Task 1 ----

Read comjson.json

Sample data

{
	"orgName": "Zeyobron",
	"trainer": "Sai",
	"amount": 40,
	"address": {
		"PermanentAddress": "Hyderabad",
		"TemporaryAddress": "Chennai"
	}
}



Flatten it completely -- All the fields should be flattened

 |-- orgName: string (nullable = true)
 |-- trainer: string (nullable = true)
 |-- amount: long (nullable = true)
 |-- PermanentAddress: string (nullable = true)
 |-- TemporaryAddress: string (nullable = true)

Generate back to the same hierarchy

 |-- orgName: string (nullable = true)
 |-- trainer: string (nullable = true)
 |-- amount: long (nullable = true)
 |-- address: struct (nullable = false)
 |    |-- PermanentAddress: string (nullable = true)
 |    |-- TemporaryAddress: string (nullable = true)


Optional
*/
			print("-----------Fetch the data----")
			
val jsonDf =spark.read.format("json")
.option("mlutiline", "true")
.load("file:///D:/BigData/Data/common.json")


jsonDf.show()

jsonDf.printSchema()

/*val jsnonDFFlt = jsonDf.select("orgName",
                                "trainer",
                                "amount", 
                                "address.PermanentAddress",
                                "address.TemporaryAddress")
                                
                                jsnonDFFlt.show()
                                
                                jsnonDFFlt.printSchema()*/
/*

Task 2 --

Take two datasets.. join1.csv and join2.csv
Perform (inner,left,right,outer,left_anti,left_semi)

df.join(df2,df("txnno")===df2("tno"),"left_anti")
     * 
     */
			
  }
  
}