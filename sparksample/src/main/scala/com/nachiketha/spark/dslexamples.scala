package com.nachiketha.spark

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._


object dslexamples {

	def main(args: Array[String]): Unit = {

			println()
			println("---------------Intiate the SPARK Conf---------------")
			println()

			val conf = new SparkConf().setAppName("Row RDD Mapping").setMaster("local[*]")
			val sc =  new SparkContext(conf)
			sc.setLogLevel("ERROR")

			val spark  = SparkSession.builder().getOrCreate()
			import spark.implicits._


			val df = spark.read.format("csv")
			              .option("header", "true")
			              .load("file:///D:/BigData/Data/txns_head")


			              df.persist()
			              df.show(50)
			              
			      /*    val filterDF=    df.filter(col("spendby") === "cash"
			              && col("category") ==="Gymnastics")
			              
			              filterDF.show(50)
			             
			              
			              val filterDF1=    df.filter(
			                  col("spendby") === "cash"
			              
			              
			              )
			              
			              filterDF1.show()
			              
			      /*  val filDf = df.filter(
			            col("category")
			            . isin ("Gymnastics","Team Sports")
			            && col("product") like "%Gymnastics%")

			              filDf.show(50)*/
			              
			    /*  val selectDF = df.select(  "txnno",
					  "txndate",
					  "custno",
					  "amount",
					  "category",
					  "product",
					  "city",
					  "state",
					  "spendby",
					  "case when spendby = 'credit' then 1 else 0 end as check")
					  
					  selectDF.show(50)*/
			              
					  
					//  Using selectExpr add an extra column at the as Check with spendby=credit as 1 else 0   ---> It will succeed



	val condf =df.selectExpr(
					  "txnno",
					  "txndate",
					  "custno",
					  "amount",
					  "category",
					  "product",
					  "city",
					  "state",
					  "spendby",
					  "case when spendby = 'credit' then 1 else 0 end as check"
					  )
				
			condf.show(50)



//Use With column to perform the sam operation


	val withcolum =condf.withColumn("check",expr("case when spendby = 'credit' then 1 else 0 end as check") )
					
					
					withcolum.show(50)
			              
			          */
			              
			              
			            //  Task 1 -------

//Read txns_head
//Using selectExpr -- split and get the year
		val condfSplit =df.selectExpr(
					  "txnno",
					  "split(txndate,'-')[2] as year",
					  "custno",
					  "amount",
					  "category",
					  "product",
					  "city",
					  "state",
					  "spendby"
					  )
condfSplit.show(50)

    


//Task 2  ------ Perform the same split operation using withColumn --

		val withcloum =	df.withColumn("year",expr("split(txndate,'-')[2]"))
		
		withcloum.show(50)
		
		df.unpersist()
			              
	}

}