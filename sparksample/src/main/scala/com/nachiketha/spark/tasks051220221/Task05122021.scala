package com.nachiketha.spark.tasks051220221

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._


object Task05122021 {
  
  def main(args: Array[String]): Unit = {
    
    val sparkconf = new SparkConf().setAppName("Tasks05122021").setMaster("local[*]")
    
    val sparkContext = new SparkContext(sparkconf);
    
    sparkContext.setLogLevel("ERROR")
    
    val spark = SparkSession.builder().getOrCreate()
    
    import spark.implicits._
    
    
     /*
   * Task 1 ---

read txns_head
Attach a column as name using withColumn and hardcode that column
*/
    
   val txnsHeadDF  = spark.read
                           .format("csv")
                           .option("header", "true")
                           .load("file:///D:/BigData/Data/txns_head")
                           
    val hardDF =   txnsHeadDF.withColumn("name",lit("Yagna"))                    
    
       hardDF.show(50)

   
/*Task 2 ---


have join1.csv and join2.csv data 
read using df1 and df2 with header true
join both the tables using txnno
val joindf= df1.join(df2,Seq("txnno"),"inner")
joindf.show()
					
*/

      val join1df =  spark.read
                          .format("csv")
                          .option("header", "true")
                     .load("file:///D:/BigData/Data/join1.csv")
                          
             join1df.show()  
                     val join2df =  spark.read
                          .format("csv")
                          .option("header", "true")
                          .load("file:///D:/BigData/Data/join2.csv")             
      
                          join2df.show()
val joindf= join1df.join(join2df,Seq("txnno"),"inner")
joindf.show(50)

/*

Task 3 ---- 

Types of Join in spark -- One liner definition

Inner,
left,
right join
left anti
left semi join
   */
    
    
  }
  
}