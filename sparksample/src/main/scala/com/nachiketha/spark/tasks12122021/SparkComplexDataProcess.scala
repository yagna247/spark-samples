package com.nachiketha.spark.tasks12122021

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql._
import org.apache.spark.sql.Row
import scala.io.Source._

object SparkComplexDataProcess {

  def main(args: Array[String]): Unit = {

    val sparkConf = new SparkConf().setAppName("complex Processing")
      .setMaster("local[*]")

    val sparkContext = new SparkContext(sparkConf).setLogLevel("ERROR")

    val spark = SparkSession.builder().getOrCreate()
    import spark.implicits._

    val urldata = fromURL("https://randomuser.me/api").mkString

    val urlrdd = spark.sparkContext.parallelize(List(urldata))

    val df = spark.read.json(urlrdd)

    df.printSchema

    val explodedf = df.withColumn("results", explode(col("results")))
    explodedf.printSchema()
    val flattendf = explodedf.select(
      "info.*",
      "results.cell",
      "results.dob.*",
      "results.email",
      "results.gender",
      "results.id.*",
      "results.location.city",
      "results.location.coordinates.*",
      "results.location.country",
      "results.location.postcode",
      "results.location.state",
      "results.location.street.*",
      "results.location.timezone.*",
      "results.login.*",
      "results.name.*",
      "results.nat",
      "results.phone",
      "results.picture.*",
      "results.registered.*")
    flattendf.printSchema()
    flattendf.show()
  }
}