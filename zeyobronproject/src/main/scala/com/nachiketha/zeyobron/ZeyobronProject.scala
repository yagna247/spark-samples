package com.nachiketha.zeyobron

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

import scala.io.Source._
import java.time.ZonedDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter

object ZeyobronProject {

  def main(args: Array[String]): Unit = {

    /*
    * 1) Eclipse create project-- spark context,spark session initialization
2) Read raw data avro after adding databricks jar
3) Read data from webapi -- https://randomuser.me/api/0.8/?results=100
4) Flatten the data completely and ignore these columns FAVS,HETU,INSEE,TFN,NINO,SSN,BSN,PPS --- Dont select during you flatten
5) Remove numericals from the username column
6) Do left broadcast join between (avro dataframe and numericals removed webapi dataframe) --- left table should avro dataframe

avrodf.join(broadcast(webapidf), Seq("username"),"left")

7) Create two dataframes from Joined dataframe
              7a) Nationality is null ---not available customers
              7b) Nationality is not null ---- available customers

8) Take Not available customer dataframe ---- find a way to replace
                          all string column nulls with  "Not available"
                          all non string columns nulls with 0
9) Take available customer dataframe and take null handled not available customer dataframe
add one column at the last with current_date with today's date
    */

    //1) Eclipse create project-- spark context,spark session initialization

    println("============1) Eclipse create project-- spark context,spark session initialization==========")
    val sparkConf = new SparkConf().setAppName("zeyobron Project").setMaster("local[*]")

    val sparkContext = new SparkContext(sparkConf)
    sparkContext.setLogLevel("ERROR")
    

    val spark = SparkSession.builder().getOrCreate()

    import spark.implicits._

    println("===========/2) Read raw data avro after adding databricks jar==========")
    //2) Read raw data avro after adding databricks jar

    val avroReadDF = spark.read.format("com.databricks.spark.avro").load("file:///D:/BigData/Spark/Project/projectsample.avro")

    avroReadDF.show(10)
    avroReadDF.printSchema()

    println("===========3) Read data from webapi -- https://randomuser.me/api/0.8/?results=100==========")
    val webapiStr = fromURL("https://randomuser.me/api/0.8/?results=100").mkString

    val paralizedUrlData = spark.sparkContext.parallelize(List(webapiStr))
    val webapiData = spark.read.json(paralizedUrlData)

    webapiData.show(10, false)
    webapiData.printSchema()

    println("===========4) Flatten the data completely and ignore these columns FAVS,HETU,INSEE,TFN,NINO,SSN,BSN,PPS --- Dont select during you flatten==========")

    val flattenWebAPIData = webapiData.withColumn("results", explode(col("results")))

    val listData = flattenWebAPIData.select(
      col("nationality"),

      col("results.user.cell"),
      col("results.user.dob"),
      col("results.user.email"),
      col("results.user.gender"),
      col("results.user.location.*"),
      col("results.user.name.*"),
      col("results.user.username"),
      col("results.user.phone"),
      col("results.user.password"))

    listData.show(10, false)
    listData.printSchema()

    println("===========5) Remove numericals from the username column==========")

    val updateUserNameDF = listData.withColumn("username", regexp_replace(col("username"), "[0-9]", ""))

    updateUserNameDF.show(10, false)
    updateUserNameDF.printSchema()

    println("===========6) Do left broadcast join between (avro dataframe and numericals removed webapi dataframe) --- left table should avro dataframe==========")

    val joindf = avroReadDF.join(broadcast(updateUserNameDF), Seq("username"), "left")
    joindf.show()

    println("===========7)Create two dataframes from Joined dataframe")
    println("7a) Nationality is null ---not available customers==========")
    val nationalityNullDF = joindf.filter(col("nationality").isNull)
    nationalityNullDF.show(10)

    println("7b) Nationality is not null ---- available customers==========")
    val nationalityNotNullDF = joindf.filter(col("nationality").isNotNull)
    nationalityNotNullDF.show(10)
    
     println("===========8) Take Not available customer dataframe ---- find a way to replace"+
                         " all string column nulls with  \"Not available\"")

                       println( " all non string columns nulls with 0==========")
                       
                val replaceNullDF =   nationalityNullDF.na.fill("Not available").na.fill(0)
                
                replaceNullDF.show(10)
                
                 println("===========9) Take available customer dataframe and take null handled not available customer dataframe"+
"add one column at the last with current_date with today's date")

                       
        val curDate1 = replaceNullDF.withColumn("current_date",current_date().as("current_date"))
        curDate1.show()
        val curDate2 = nationalityNotNullDF.withColumn("current_date",current_date().as("current_date"))
        curDate2.show()
        
        println("Project Phase 2------1) Read Yesterday's avro Data Dynamically")


         val today = ZonedDateTime.now(ZoneId.of("UTC"))
val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
val todaydate = formatter format today
				
val data = spark.read.format("com.databricks.spark.avro").load(s"file:///D:/BigData/Data/proddata/$todaydate/")
data.show()   
                       
   val indexeddf = curDate1.withColumn("index", monotonically_increasing_id()).show()

  }
}