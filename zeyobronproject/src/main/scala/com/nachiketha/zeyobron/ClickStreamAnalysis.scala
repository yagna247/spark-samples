package com.nachiketha.zeyobron

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.plans.logical.Window._
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._

object ClickStreamAnalysis {
  
  
 def main(args: Array[String]): Unit = {
   
   val sparkConf =  new SparkConf()
                       .setAppName("click stream")
                       .setMaster("local[*]")
                       
   val sparkContext = new SparkContext(sparkConf)
         sparkContext.setLogLevel("ERROR")
   
   
    val spark = SparkSession.builder().getOrCreate()
    import spark.implicits._
    
    val intialDF =    spark.read.format("csv").option("header", "true").load("file:///D://BigData//Data//Clicks.csv")
    
   
    
    intialDF.show()
    
  val windowSpec =    Window.partitionBy($"user").orderBy($"click_time")
  
  val windowdf = intialDF.withColumn("lagvalue", lag($"click_time" ,2).over(windowSpec))
  
  windowdf.show()
  
  val df3 = windowdf.withColumn("tsdiff", ((unix_timestamp($"click_time") - unix_timestamp($"lagvalue"))))
      
  val df4 = df3.withColumn("tsdiff", when($"tsdiff".isNull, 0).otherwise($"tsDiff"))
  
  val df5 = df4.withColumn("SessionName", when($"tsDiff" >30 , 1).otherwise(0))
  
  df5.show()
  
                       
                       
   
   
 }
  
}