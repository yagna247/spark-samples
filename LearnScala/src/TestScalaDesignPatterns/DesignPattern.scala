package TestScalaDesignPatterns

import scala.collection._
object DesignPattern extends App {
  
  
  def duplicate  (input: String ){
    
    val splitList = input.split(" ").toList
    
  val list4 =  splitList.map(str => str.split(","))

  
    list4.mkString(",").foreach(print)
  }
  
 // def main(args: Array[String]): Unit = {
    
    //Given the below two list list 1 and list 2.
 //Write a Python/Scala code to print the first list in the original order and the second list in the reverse order simultaneously.
    
  val list1 =  List(1,2,5,7,9)
    
    val list2 =List(22,44,11,55,22)
    
 // val list3 =  list1.reverse ++ list2
  
    val  list3 = list1.reverse
  // print( list3.mkString(",")+","+list2.mkString(","))
  
    (list3,list2).zipped.map{(l3 ,l1) => println(l3,l1)}
  
//Write a function in Python/Scala which can give the frequency of each word in a string?
  val input_string = "the cat and the dog are fighting"

  duplicate(input_string)

  
 // }
}